# nexus3-export-import

## overview
* needed a way to move artifacts from one nexus3 oss installation to another nexus3 oss installation.
* developers at sonatype are working on a ui version of this [NEXUS-11468](https://issues.sonatype.org/browse/NEXUS-11468).
* note: these instructions should work for most macosx (tested) and linux devices

## news 
* 11-apr-2020 : switched to groovy 3.0.3 (take advantage of junit5)

## video guide
* [_youtube video: nexus3-export-import_](https://youtu.be/GJPo8JSwpQ8)

## install/setup
* make sure you have [groovy](http://groovy.apache.org/) installed and on your path
```
$ which groovy
/usr/local/bin/groovy
```

* pull the project from the bitbucket site and go into the directory
```
$ git clone https://javapda@bitbucket.org/javapda/nexus3-export-import.git
$ cd nexus3-export-import
```

* test help
```
  ./nexus3ExportImport.groovy --help

  start:    Thu Apr 09 22:31:58 MST 2020
  args: [--nexus3InstallationName, localhost-fake, --help]
  usage: nexus3ExportImport.groovy [<options>]
  Options:
      --artifactPackagingFilters <csv-packaging>    csv list (e.g. pom,jar,war
  -h,--help                                        Print this help text and exit.
      --nexus3InstallationName <install-name>       name of nexus3 installation
      --saveArtifacts                               if true will remain after processing,
                                                    else cleaned up right away
      --sourceNexus3Url <url>                       main nexus url of source repo [e.g.
                                                    https://my.domain.com/nexus]
      --sourceRepoCredentials <username:password>   authentication credentials
      --sourceRepoName <name>                       name of source repo [e.g. releases]
      --sourceSearch <regex to search>              regex of artifact or group
      --targetNexus3Url <url>                       main nexus url of source repo [e.g.
                                                    https://my.domain.com/nexus]
      --targetRepoCredentials <username:password>   authentication credentials
      --targetRepoName <name>                       name of source repo [e.g. releases]
      --targetSearch <regex to search>              regex of artifact or group
      --tempDir <path>                              where to store artifacts locally before
                                                    transfer
      --verbose <enabled>                           true or false, if true more output

  Preferences file:
  File '/Users/jkroub/.nexus-export-import-prefs' (exists)
  Can be used to store preferences in lieu of the command-line arguments below
  Examples:
  ./nexus3ExportImport.groovy  \
  --sourceRepoName "releases" \
  --sourceNexus3Url http://localhost:8089 \
  --verbose true \
  --sourceRepoCredentials "jed:wilma" \
  --targetNexus3Url "http://nexus:8083" \
  --targetRepoName "releases"
  -------------------------------------------------
  ./nexus3ExportImport.groovy --help      # see this help
  -------------------------------------------------
  ./nexus3ExportImport.groovy  \
  --nexus3InstallationName  "localhost-fake" \
  --verbose true
  -------------------------------------------------
```
* use preferences file : ${HOME}/.nexus3-export-import.yaml
  * you can use the command-line options described above, but it has proven much more manageable to use the preferences yaml file that you will create
```
cd ${HOME}
vim .nexus3-export-import.yaml
```
  * add the following content to _.nexus3-export-import.yaml_ and save
```
# preferences for nexus3-export-import

description: "preferences for nexus3-export-import"
installations:
    - name : localhost-fake
      nexus3Url : http://localhost:8089
      credentialsText : fake:sPJ4YlrztDEu
    - name : nexus-fake
      nexus3Url : https://nexus.a_company.com/nexus
      credentialsText : fake:G5y5t3m5

source:
  repoName : thirdparty
  nexus3Url : http://localhost:8089
  credentials : fake:sPJ4YlrztDEu
  verbose: true

target:
  repoName : fake-repo
  nexus3Url : http://localhost:8089
  credentials : fake:sPJ4YlrztDEu
  verbose: true
  
tempDir: /tmp/artifact-cache
verbose: true
artifactPackagingFilters: [
  pom,jar]    
  # pom,jar,war]
```

* the *source* and *target* sections are the interesting portions. you will modify these sections to suit your needs. For example, if your NXRM supports anonymous access, simply remove the credentials pieces. By the way, the credentials are specified as a username and password separated by a colon.


## related
* [NEXUS-11468](https://issues.sonatype.org/browse/NEXUS-11468)
* [issue with httpbuilder / xerces](https://stackoverflow.com/questions/37559458/groovy-install-httpbuilder-now-that-codehaus-shutdown)

## usage
```
chmod a+x nexus3ExportImport.groovy
./nexus3ExportImport.groovy
```

## local links
* [secure maven](docs/secure-maven.md)
* [expand vm filesystem](docs/expand-vm-filesystem.md)

## links
* [sonatype nexus oss](https://www.sonatype.com/nexus-repository-oss) | [download](https://www.sonatype.com/download-oss-sonatype) | [docs](https://help.sonatype.com/repomanager3) | [REST API](https://help.sonatype.com/repomanager3/rest-and-integration-api)
* [@ToString](https://docs.groovy-lang.org/latest/html/gapi/groovy/transform/ToString.html)
* [groovy operators, elvis ?:](http://groovy-lang.org/operators.html#_elvis_operator)
* [NEXUS-11468](https://issues.sonatype.org/browse/NEXUS-11468)
* [groovy regex](https://e.printstacktrace.blog/groovy-regular-expressions-the-definitive-guide/)
* [groovy file read](https://www.baeldung.com/groovy-file-read)
* [groovy json](http://groovy-lang.org/json.html)
* [groovy submap](https://mrhaki.blogspot.com/2009/10/groovy-goodness-getting-submap-from-map.html)
* [groovy testing](https://docs.groovy-lang.org/docs/latest/html/documentation/core-testing-guide.html)
* [groovy add maps to maps](https://mrhaki.blogspot.com/2010/04/groovy-goodness-adding-maps-to-map_21.html)
* [groovy Parse YAML With YamlSlurper](https://mrhaki.blogspot.com/2020/02/groovy-goodness-parse-yaml-with.html)
* [groovy Style Guide](https://groovy-lang.org/style-guide.html)
* [groovy javadoc](http://groovy-lang.org/gdk.html)
* [maven pom.xml](https://maven.apache.org/pom.html)
* [maven settings.xml](https://maven.apache.org/settings.html)
* [regex cheat sheet](https://gist.github.com/vitorbritto/9ff58ef998100b8f19a0)
* [Visual Studio Code - Groovy](https://www.youtube.com/watch?v=bh2zQkJFPmE)
* [Step-by-Step Videos](https://www.youtube.com/channel/UCTt7pyY-o0eltq14glaG5dg)
* [http-builder](https://github.com/jgritman/httpbuilder)


## Errors encountered
* be careful with numbers in Groovy as they can morph behind the scenes into BigDecimal, which is not compatible with all math operations (e.g. modulus)
```
testDuration(com.javapda.nexus3.util.time.DurationUtilTest)java.lang.UnsupportedOperationException: 
Cannot use mod() on this number type: java.math.BigDecimal with value: 1.036

- or - 

 testDuration(com.javapda.nexus3.util.time.DurationUtilTest)java.util.IllegalFormatConversionException: d != java.math.BigDecimal
```