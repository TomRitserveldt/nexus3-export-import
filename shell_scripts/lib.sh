###################################
# assign some basic variables
###################################
# File name
readonly _progname=$(basename $0)
# File name, without the extension
readonly _progbasename=${_progname%.*}
# File directory
readonly _progdir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
# Arguments
readonly _args="$@"
# Arguments number
readonly _argnum="$#"


############################################################
# _nexusVersion "nexusUrl"
#  - retrieves the current version of nexus
############################################################
_nexusVersion() {
  [[ "${#}" -eq 1 ]] || abort "_nexusVersion expected 1 args, found '${#}'"
  local nexus3Url="${1}"
  local cmd="curl --silent -i -X GET \"${nexus3Url}/service/rest/v1/status\" -H \"accept: application/json\" | grep Nexus "
  [[ -z "${verbose}" ]] || echo "cmd: ${cmd}"
  local versionText=$(eval "${cmd}")
  local regex="([:0-9a-zA-Z]*): (Nexus)\/([0-9]+\.[0-9]+\.[0-9]+-[0-9]+) \(OSS\)"
  if [[ ${versionText} =~ ${regex} ]]
  then
      echo "${BASH_REMATCH[3]}"
  else
      abort "cannot locate nexus version: versionText=${versionText}"
  fi

}