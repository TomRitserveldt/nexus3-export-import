package com.javapda.misc
// @Grab('org.snakeyaml:snakeyaml-engine:2.1')
@Grapes([
    @Grab(group='org.yaml', module='snakeyaml', version='1.26'),
    @Grab(group='org.snakeyaml', module='snakeyaml-engine', version='1.0')
    // @Grab(group='org.snakeyaml', module='snakeyaml-engine', version='2.1' )
    ]
)
// @Grab(group='org.snakeyaml', module='snakeyaml-engine', version='2.1' )
import groovy.transform.ToString

import org.yaml.snakeyaml.Yaml
import org.snakeyaml.engine.v1.api.LoadSettings
import org.snakeyaml.engine.v1.api.LoadSettingsBuilder
import org.snakeyaml.engine.v1.api.Load

/**
groovy -Dgroovy.grape.report.downloads=true com/javapda/misc/YamlTest.groovy
groovy -Dgroovy.grape.report.downloads=true -Divy.message.logger.level=4 com/javapda/misc/YamlTest.groovy

https://stackoverflow.com/questions/41731059/how-do-i-parse-a-yaml-file-in-groovy
https://bitbucket.org/asomov/snakeyaml-engine/src/master/
YAML is a data serialization format designed for human readability and interaction with scripting languages.
https://bitbucket.org/asomov/snakeyaml-engine/wiki/Documentation
SnakeYAML Engine is a YAML 1.2 processor for the Java Virtual Machine version 8 and higher.
*/
class YamlTest extends GroovyTestCase {

    void testEmpty() {
        Yaml yaml = new Yaml()
        println(yaml.load(my_yaml))
        Customer cust = yaml.load(my_yaml)
        println(cust)

        StringWriter writer = new StringWriter();
        yaml.dump(cust, writer);
        String genYaml = writer.toString()
        println("genYaml: ${genYaml}")
        def pieces = genYaml.split("\\n")
        println("pieces: ${pieces}")
        println(pieces[1..-2])

    }


    static XXmy_yaml="""
firstName: "John"
lastName: "Doe"
age: 31
"""    
    static my_yaml="""
lastName: Doe    
age: 31
contactDetails:
- {type: mobile, number: 123456789}
- {type: landline, number: 456786868}
- {type: work, number: 123786868}
firstName: John
homeAddress:
  
    """

    static Xmy_yaml="""
firstName: "John"
lastName: "Doe"
age: 31
contactDetails:
   - type: "mobile"
     number: 123456789
   - type: "landline"
     number: 456786868
   - type: "work"
     number: 123786868
homeAddress:
   line: "Xyz, DEF Street"
   city: "City Y"
   state: "State Y"
   zip: 345657    """
}


@ToString(ignoreNulls=false,includeNames=true, includeFields=true )
public class Customer {
 
    private String firstName;
    private String lastName;
    private int age;
    private List<Contact> contactDetails;
    private Address homeAddress;     
    // getters and setters
    public String getFirstName() {
        return firstName
    }
    public String getLastName() {
        return lastName
    }
    public int getAge() {
        return age
    }
    public void setFirstName(String fn) {
        this.firstName = fn
    }
    public void setLastName(String fn) {
        this.lastName = fn
    }
    public void setAge(int fn) {
        this.age = fn
    }
    public List<Contact> getContactDetails() {
        return this.contactDetails
    }
    public void setContactDetails(List<Contact> contactDetails) {
        this.contactDetails = contactDetails
    }
    public Address getHomeAddress() {
        return this.homeAddress
    }
    public void setHomeAddress(Address homeAddress) {
        this.homeAddress=homeAddress
    }

}

@ToString(ignoreNulls=false,includeNames=true, includeFields=true )
public class Contact {
    private String type;
    private int number;
    // getters and setters
}

@ToString(ignoreNulls=false,includeNames=true, includeFields=true )
public class Address {
    private String line;
    private String city;
    private String state;
    private Integer zip;
    // getters and setters
}