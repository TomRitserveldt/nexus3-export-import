package com.javapda.nexus3.nei.cli
import com.javapda.nexus3.nei.domain.*

enum Nexus3ExportImportCLI {
    INSTANCE
    static final String PROG_NAME="nexus3ExportImport.groovy"

    CliBuilder cliBuilder
    def footer() {
        """
         Preferences file:
           File '${System.properties['user.home']}/.nexus-export-import-prefs' (${new File(System.properties['user.home'],'.nexus-export-import-prefs').exists()?'exists':'dose not exist'})
           Can be used to store preferences in lieu of the command-line arguments below

         Examples:

         ./${PROG_NAME}  \\
            --sourceRepoName "releases" \\
            --sourceNexus3Url http://localhost:8089 \\
            --verbose true \\
            --sourceRepoCredentials "jed:wilma" \\
            --targetNexus3Url "http://nexus:8083" \\
            --targetRepoName "releases"

            -------------------------------------------------
         ./${PROG_NAME} --help      # see this help
            -------------------------------------------------
         ./${PROG_NAME}  \\
            --nexus3InstallationName  "localhost-fake" \\
            --verbose true
            -------------------------------------------------

        """
    }

    Nexus3ExportImportCLI() {
        cliBuilder = new CliBuilder(
                usage: "${PROG_NAME} [<options>]",
                header: 'Options:',
                footer: footer()
        )
        // set the amount of columns the usage message will be wide
        cliBuilder.width = 90  // default is 74
        cliBuilder.with {
            _(longOpt: 'nexus3InstallationName',args:1,argName:'install-name',required:false,'name of nexus3 installation')
            _(longOpt: 'sourceNexus3Url',args:1,argName:'url',required:false,'main nexus url of source repo [e.g. https://my.domain.com/nexus]')
            _(longOpt: 'sourceRepoName',args:1,argName:'name',required:false,'name of source repo [e.g. releases]')
            _(longOpt: 'sourceRepoCredentials',args:1,argName:'username:password',type: GString,required:false,'authentication credentials')
            _(longOpt: 'sourceSearch',args:1,argName:'regex to search',required:false,'regex of artifact or group')
            _(longOpt: 'targetNexus3Url',args:1,argName:'url',required:false,'main nexus url of source repo [e.g. https://my.domain.com/nexus]')
            _(longOpt: 'targetRepoName',args:1,argName:'name',required:false,'name of source repo [e.g. releases]')
            _(longOpt: 'targetRepoCredentials',args:1,argName:'username:password',type: GString,required:false,'authentication credentials')
            _(longOpt: 'targetSearch',args:1,argName:'regex to search',required:false,'regex of artifact or group')
            _(longOpt: 'tempDir',args:1,argName:'path',required:false,'where to store artifacts locally before transfer')
            _(longOpt: 'saveArtifacts',required:false,'if true will remain after processing, else cleaned up right away')
            _(longOpt: 'artifactPackagingFilters',args:1,argName:'csv-packaging',required:false,'csv list (e.g. pom,jar,war')
            h longOpt: 'help', 'Print this help text and exit.'
            _(longOpt: 'verbose',args:1,argName:'enabled', 'true or false, if true more output')
        }
    }

    CopyNexusRepoConfig parse(args) {
        OptionAccessor options = cliBuilder.parse(args)

        if (!options) {
            System.err << 'Error while parsing command-line options.\n'
            System.exit 1
        }
        

        if (options.h) {
            cliBuilder.usage()
            System.exit 0
        }
        Nexus3Config source,target
        Boolean verbose
        List<String> artifactPackagingFilters
        File tempDir

        def Nexus3ExportImportPreferences preferences
        preferences = Nexus3ExportImportPreferences.loadLocal()
        if (preferences) {
            verbose = preferences.verbose
            source = preferences.source
            target = preferences.target
            artifactPackagingFilters=preferences.artifactPackagingFilters
            if (preferences.tempDir) {
                tempDir = new File(preferences.tempDir)
            }
        }

        source = source?:new Nexus3Config(nexus3Url:options.sourceNexus3Url,repoName:options.sourceRepoName,credentials:new Credentials(options.sourceRepoCredentials?options.sourceRepoCredentials:null))
        target = target?:new Nexus3Config(nexus3Url:options.targetNexus3Url,repoName:options.targetRepoName,credentials:new Credentials(options.targetRepoCredentials?options.targetRepoCredentials:null))
        verbose = verbose?:Boolean.valueOf(options.verbose)
        tempDir = tempDir?tempDir:(options.tempDir?new File(options.tempDir):new File("."))
        artifactPackagingFilters=artifactPackagingFilters?:(options.artifactPackagingFilters?options.artifactPackagingFilters.split(","):null)
        CopyNexusRepoConfig c = 
            new CopyNexusRepoConfig(
                    source:source,
                    target:target,
                    verbose:verbose,
                    tempDir:tempDir,
                    artifactPackagingFilters:artifactPackagingFilters
                    )
        return c
    }
}