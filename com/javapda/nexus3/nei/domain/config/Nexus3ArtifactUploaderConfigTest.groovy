package com.javapda.nexus3.nei.domain.config
import com.javapda.nexus3.nei.domain.*
import groovy.transform.ToString
import com.javapda.nexus3.util.*

@ToString(includeNames=true, includeFields=true)
class Nexus3ArtifactUploaderConfigTest extends GroovyTestCase {
    String repoUrl,repoName,filename
    Credentials credentials
    GAV gav
    Boolean verbose

    void test1() {
        assert validConfig().verify()
    }

    Nexus3ArtifactUploaderConfig validConfig() {
         new Nexus3ArtifactUploaderConfig(
             repoUrl:"http://cnn.com",
             gav:GAV.create("org.apache.commons:commons-lang3:3.10::jar").verify(),
             repoName:"thirdparty", 
             filename:new File("./com/javapda/nexus3/testdata/fake.md5").absolutePath)
    }
}