package com.javapda.nexus3.nei.domain

import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true, includePackage=false)
class Semver {
    String semverText, suffix
    Integer major,minor,patch

}