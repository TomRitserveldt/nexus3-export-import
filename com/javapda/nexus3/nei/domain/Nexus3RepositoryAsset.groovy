package com.javapda.nexus3.nei.domain
import groovy.json.JsonSlurper
import com.javapda.nexus3.util.*
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class Nexus3RepositoryAsset {
   String downloadUrl, path, id, repository, format
   Checksum checksum
   boolean isValidPath() {
        try {
            getPathInfo();
        } catch(e) {
            return false
        }
        return true

   }
   def NexusArtifactPathInfo getPathInfo() {
       new NexusArtifactPathParser().parse(path)
   }
   public Semver getSemver() {
       new SemverParser().parse(this.gav.version)
   }
   public GAV getGav() {
        this.pathInfo.gav
   }
    static Nexus3RepositoryAsset fromMap(Map map) {
        new Nexus3RepositoryAsset(
            downloadUrl:map.downloadUrl,
            path:map.path,
            id:map.id,
            repository:map.repository,
            format:map.format,
            checksum:Checksum.fromMap(map.checksum)
        )
    }
    static Nexus3RepositoryAsset fromJson(String json) {
        Map<String, Object> map = new JsonSlurper().parseText(json)
        def submap=map.subMap(['downloadUrl','path','id','repostory','format'])
        Checksum checksum = map.checksum?new Checksum(map.checksum):null
        submap << [checksum:checksum]
        new Nexus3RepositoryAsset(submap)
    }

}