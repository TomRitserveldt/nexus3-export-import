package com.javapda.nexus3.nei.domain
import groovy.transform.ToString
import com.javapda.nexus3.nei.domain.*
@ToString(includeNames=true, includeFields=true)
class Nexus3Repo {
    String name,format,url,type
    Boolean online
    Nexus3RepoStorage storage
    Nexus3RepoGroup group
    List<String> attributes
    
}