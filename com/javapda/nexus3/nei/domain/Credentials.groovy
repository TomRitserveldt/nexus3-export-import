package com.javapda.nexus3.nei.domain
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class Credentials {
    public String username, password
    Credentials(String colonSeparatedUsernamePassword) {
        if (colonSeparatedUsernamePassword!=null) {
            def pieces = colonSeparatedUsernamePassword.split(":")
            username=pieces[0].trim()
            password=pieces[1].trim()
        }
    }
    String getColonSeparatedText() {
        return "${username}:${password}"
    }
    boolean isEmpty() {
        return username==null && password==null
    }

    String getBasicAuth() {
        return (username && password) ? "Basic " + new String(Base64.encoder.encode(getColonSeparatedText().getBytes())) : null
    }
}