package com.javapda.nexus3.nei.domain
import groovy.json.JsonSlurper
import groovy.transform.ToString

@ToString(ignoreNulls=false,includeNames=true, includeFields=true )
class ChecksumTest extends GroovyTestCase { 

    void testFromMap() {
        def map = [sha1:'some-sha1',md5:'some-md5']
        Checksum checksum = Checksum.fromMap(map)
        assert checksum
        assert checksum.sha1
        assert checksum.md5
        // println(checksum)
    }
    void testFromJson() {
        // println("json: ${json}")
        Object obj = new JsonSlurper().parseText(json)
        assert obj instanceof Map
        assert obj.sha1 
        assert obj.md5 
        assert Checksum.fromJson(json) instanceof Checksum
        // println(Checksum.fromJson(json))

    }


    static json='''
            {
                "sha1" : "2561ae58469442f00018de28cb4b912442281427",
                "md5" : "d6ec9da10e1fb030295ad43b9e5bf010"
            }    
    '''
}