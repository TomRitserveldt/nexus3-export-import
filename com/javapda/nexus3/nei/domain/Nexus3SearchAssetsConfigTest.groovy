package com.javapda.nexus3.nei.domain
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class Nexus3SearchAssetsConfigTest extends GroovyTestCase {
    void testQueryString() {
        Nexus3SearchAssetsConfig config = new Nexus3SearchAssetsConfig(mavenAssetConfig:mavenAssetConfig(),repoName:"billyGoatRepo")
        assert config
        assert config.mavenAssetConfig
        assert config.asQueryString()
        assert "repository=billyGoatRepo&maven.groupId=com.gs&maven.artifactId=gs-util&maven.baseVersion=8.0.9.RELEASE&maven.extension=jar" == config.asQueryString()
    }
    MavenAssetConfig mavenAssetConfig() {
        new MavenAssetConfig(groupId:'com.gs',artifactId:'gs-util',baseVersion:'8.0.9.RELEASE',extension:'jar',classifier:null)
    }
}
