package com.javapda.nexus3.nei.domain
import com.javapda.nexus3.nei.action.*
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class Nexus3ConfigTest extends GroovyTestCase {
    Credentials credentials
    Nexus3Installation installation


    void test() {
        assert installation()
        assert credentials()
        assert new Nexus3Config(nexus3Url:nexus3Url(),repoName:null,credentials:credentials(),verbose:true).verify()
        assert new Nexus3Config(nexus3Url:nexus3Url(),repoName:"releases",credentials:credentials(),verbose:true).verify()
        assert Nexus3Config.create(installation(),"releases")
    }

    String nexus3Url() {
        assert installation().nexus3Url
        installation().nexus3Url
    }

    Nexus3Installation installation() {
        if (!installation) {
            installation = Nexus3ExportImportPreferences.loadLocal().getInstallation("localhost-fake")
            assert installation
        }
        installation
    }

    Credentials credentials() {
        if (!credentials) {
            credentials = installation().credentials
            assert credentials
        }
        credentials
    }

}