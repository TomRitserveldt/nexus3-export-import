package com.javapda.nexus3.nei.domain
import groovy.json.JsonSlurper
import com.javapda.nexus3.testdata.*
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class Nexus3RepositoryAssetsTest extends GroovyTestCase {
  void testPreferences() {
    
  }
  void testFromJson() {
      println("json: ${json}")
      Object obj = new JsonSlurper().parseText(json)
      // println("obj: ${obj}")
      assert obj instanceof Map
      assert obj.items 
      assert obj.items instanceof List
      assert Nexus3RepositoryAssets.fromJson(json) instanceof Nexus3RepositoryAssets
      
      assert Nexus3RepositoryAssets.fromJson(json).continuationToken
      assert Nexus3RepositoryAssets.fromJson(json).items!=null
    
  }

      static String nexusUrl="http://localhost:8089"
      static json = """
      {
    "items" : [ {
      "downloadUrl" : "${nexusUrl}/repository/thirdparty/archetype-catalog.xml.sha1",
      "path" : "archetype-catalog.xml.sha1",
      "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmODI3MzMxMDUzMmIwMjZiNg",
      "repository" : "thirdparty",
      "format" : "maven2",
      "checksum" : {
        "sha1" : "cbbeadfeff8ccf532e1acacc429f37e539ca99f3",
        "md5" : "3974c83a65b771ebe7fdbf66928d01f8"
      }
    }, {
      "downloadUrl" : "${nexusUrl}/repository/thirdparty/bigfaceless/bfopdf-qrcode/2.18.8/bfopdf-qrcode-2.18.8.jar.md5",
      "path" : "bigfaceless/bfopdf-qrcode/2.18.8/bfopdf-qrcode-2.18.8.jar.md5",
      "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmYmNhZGZjN2FkMjdlYjJlYg",
      "repository" : "thirdparty",
      "format" : "maven2",
      "checksum" : {
        "sha1" : "d792c84cba92d5cfc004a9429946c3810979f172",
        "md5" : "6e92b02e638e0f5315e138a324501875"
      }
    }, {
      "downloadUrl" : "${nexusUrl}/repository/thirdparty/bigfaceless/bfopdf-qrcode/2.18.8/bfopdf-qrcode-2.18.8.pom",
      "path" : "bigfaceless/bfopdf-qrcode/2.18.8/bfopdf-qrcode-2.18.8.pom",
      "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmZGNiOWY2OGNiNTk0YTQzNg",
      "repository" : "thirdparty",
      "format" : "maven2",
      "checksum" : {
        "sha1" : "d642f1217d242efe3bd871431ceff306db50aaab",
        "md5" : "36f1850608661c7557510e07168b946d"
      }
    }, {
      "downloadUrl" : "${nexusUrl}/repository/thirdparty/bigfaceless/bfopdf-qrcode/2.18.8/bfopdf-qrcode-2.18.8.pom.sha1",
      "path" : "bigfaceless/bfopdf-qrcode/2.18.8/bfopdf-qrcode-2.18.8.pom.sha1",
      "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmYzg4MTNkMzlmODZmM2MzZQ",
      "repository" : "thirdparty",
      "format" : "maven2",
      "checksum" : {
        "sha1" : "558c2cdfb18c4e9f0561b20dd98ca784e1e7609c",
        "md5" : "46cc191f409c360a447d20ca907319eb"
      }
    }, {
      "downloadUrl" : "${nexusUrl}/repository/thirdparty/bigfaceless/bfopdf-qrcode/2.20.1/bfopdf-qrcode-2.20.1.jar.md5",
      "path" : "bigfaceless/bfopdf-qrcode/2.20.1/bfopdf-qrcode-2.20.1.jar.md5",
      "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmMWVkYzU0MWE2NDBjNjBlZg",
      "repository" : "thirdparty",
      "format" : "maven2",
      "checksum" : {
        "sha1" : "8b04b98ea2e4834725c9f87e37dfcaf4d52abb48",
        "md5" : "78af6282f34b95ecdf0a6125a2f714a3"
      }
    }, {
      "downloadUrl" : "${nexusUrl}/repository/thirdparty/bigfaceless/bfopdf-qrcode/2.20.1/bfopdf-qrcode-2.20.1.pom",
      "path" : "bigfaceless/bfopdf-qrcode/2.20.1/bfopdf-qrcode-2.20.1.pom",
      "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmYjVlMTI0OGY2YjkzY2M3MQ",
      "repository" : "thirdparty",
      "format" : "maven2",
      "checksum" : {
        "sha1" : "5c5b1fbb7d6217ef4d98d2c33da0c78cd16341fc",
        "md5" : "423eb015f3de60a46f076377075c9deb"
      }
    }, {
      "downloadUrl" : "${nexusUrl}/repository/thirdparty/bigfaceless/bfopdf-qrcode/2.20.1/bfopdf-qrcode-2.20.1.pom.sha1",
      "path" : "bigfaceless/bfopdf-qrcode/2.20.1/bfopdf-qrcode-2.20.1.pom.sha1",
      "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmMGU2YzM3MTBiOGQ0NWI3OQ",
      "repository" : "thirdparty",
      "format" : "maven2",
      "checksum" : {
        "sha1" : "53093cf1c9ab1d7587e1294f97beb094c91f8bbb",
        "md5" : "eaee6925ecead2a598ed8ccca71c1857"
      }
    }, {
      "downloadUrl" : "${nexusUrl}/repository/thirdparty/bigfaceless/bfopdf-qrcode/maven-metadata.xml.md5",
      "path" : "bigfaceless/bfopdf-qrcode/maven-metadata.xml.md5",
      "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmMGU1YWU2ZDc1MzkwYzdmYQ",
      "repository" : "thirdparty",
      "format" : "maven2",
      "checksum" : {
        "sha1" : "148f503d6a4a7088a9cb24dd9d750c19febefb69",
        "md5" : "c68bb182278811f348888c645a1359b0"
      }
    }, {
      "downloadUrl" : "${nexusUrl}/repository/thirdparty/bigfaceless/bfopdf-stamp/2.18.8/bfopdf-stamp-2.18.8.jar",
      "path" : "bigfaceless/bfopdf-stamp/2.18.8/bfopdf-stamp-2.18.8.jar",
      "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmZjEzZjVjYjcyNTc5OGI5ZA",
      "repository" : "thirdparty",
      "format" : "maven2",
      "checksum" : {
        "sha1" : "f7ad7fc097e6d052e21347eba8c15270494fffc1",
        "md5" : "f762b33667eddad5cc118e7fc7509dbd"
      }
    }, {
      "downloadUrl" : "${nexusUrl}/repository/thirdparty/bigfaceless/bfopdf-stamp/2.18.8/bfopdf-stamp-2.18.8.jar.sha1",
      "path" : "bigfaceless/bfopdf-stamp/2.18.8/bfopdf-stamp-2.18.8.jar.sha1",
      "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmNGE1ZjE0NjcwNWU0OTE0OA",
      "repository" : "thirdparty",
      "format" : "maven2",
      "checksum" : {
        "sha1" : "2561ae58469442f00018de28cb4b912442281427",
        "md5" : "d6ec9da10e1fb030295ad43b9e5bf010"
      }
    } ],
    "continuationToken" : "7f6379d32f8dd78f4a5f146705e49148"
  }
      """
}