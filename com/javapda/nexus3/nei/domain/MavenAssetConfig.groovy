package com.javapda.nexus3.nei.domain
import com.javapda.nexus3.rest.*
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class MavenAssetConfig {
    String groupId, artifactId, baseVersion, extension, classifier

    public String asQueryString() {
        def propertiesOfInterestMap = this.properties.subMap(['groupId', 'artifactId', 'baseVersion', 'extension', 'classifier']).findAll { k,v -> v}//).get()
        def map=[:]
        propertiesOfInterestMap.each { k,v -> map["maven.${k}"] = v}
        new QueryStringBuilder(params:map).get()
    }
}