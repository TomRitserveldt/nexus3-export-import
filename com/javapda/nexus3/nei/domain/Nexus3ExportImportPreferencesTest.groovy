package com.javapda.nexus3.nei.domain

import groovy.json.JsonSlurper
import groovy.transform.ToString

/**
https://stackoverflow.com/questions/35521788/snakeyaml-by-example
*/
@ToString(includeNames=true, includeFields=true)
class Nexus3ExportImportPreferencesTest extends GroovyTestCase {

    void test() {
        assert Nexus3ExportImportPreferences.loadLocal()
        assert Nexus3ExportImportPreferences.loadLocal().getInstallation("localhost-fake")
        assert Nexus3ExportImportPreferences.loadLocal().getInstallation("nexus-fake")
        assert !Nexus3ExportImportPreferences.loadLocal().getInstallation("monkey")
        
        assert Nexus3ExportImportPreferences.loadLocal(System.properties['user.home']+"/.nexus3-export-import.yaml")
        assert !Nexus3ExportImportPreferences.loadLocal("BOGUS")
        Nexus3ExportImportPreferences.loadLocal().installations.eachWithIndex {
            inst, index ->
              //println("${index} ${inst}")
        }
        println(Nexus3ExportImportPreferences.loadLocal())


    }
    
}
