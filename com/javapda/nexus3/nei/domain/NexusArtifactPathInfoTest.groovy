package com.javapda.nexus3.nei.domain
import groovy.transform.ToString


@ToString(includeNames=true, includeFields=true, includePackage=false)
class NexusArtifactPathInfoTest extends GroovyTestCase {

    void testFromJson() {
        assert NexusArtifactPathInfo.fromJson(json)
        // println(NexusArtifactPathInfo.fromJson(json))
    }

    static json = '''
        {"snapshotCounter":25,
            "underlyingPackaging":"jar",
            "dateyyyyMMdd":"20200312","release":false,
            "snapshotVersionId":"8.0.9-SNAPSHOT",
            "timeHHmm":"010211",
            "classifier":null,
            "filename":"gs-util-8.0.9-20200312.010211-25.jar.md5",
            "snapshot":true,
            "groupId":"com.gs",
            "versionId":"8.0.9",
            "path":"com/gs/gs-util/8.0.9-SNAPSHOT/gs-util-8.0.9-20200312.010211-25.jar.md5",
            "packaging":"md5",
            "artifactId":"gs-util"
            }    
    '''
}