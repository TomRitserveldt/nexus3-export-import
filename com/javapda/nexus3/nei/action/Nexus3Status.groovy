package com.javapda.nexus3.nei.action
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.rest.*
import com.javapda.nexus3.urlproviders.*

/**
Response headers
 content-length: 0 
 date: Sat, 11 Apr 2020 19:13:48 GMT 
 server: Nexus/3.22.0-02 (OSS) 
 x-content-type-options: nosniff 
curl -X GET "http://localhost:8089/service/rest/v1/status" -H "accept: application/json"

curl -i -X GET "http://localhost:8089/service/rest/v1/status" -H "accept: application/json" -u "fake:sPJ4YlrztDEu"
HTTP/1.1 200 OK
Date: Sun, 12 Apr 2020 00:06:55 GMT
Server: Nexus/3.22.0-02 (OSS)
X-Content-Type-Options: nosniff
Content-Length: 0

*/
class Nexus3Status {
    def getStatus(String nexus3Url, Credentials credentials) {
        // build url with maven search info
        def url = getStatusUrl(nexus3Url)
        Map<String, List<String>> map = new ResponseHeadersGet(url:url, credentials:credentials).get()
        return map["Server"] ?: ["unable to access nexus3 server status"]
        
    }
    private String getStatusUrl(String nexus3Url) {
            "${nexus3Url}/service/rest/v1/status"
    }

}