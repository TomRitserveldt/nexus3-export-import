package com.javapda.nexus3.nei.action
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.testdata.*

import com.javapda.nexus3.rest.*

class Nexus3SearchAssetsTest extends GroovyTestCase {
    void test() {
        TestData td = new TestData()
        assert td
        Nexus3Config nexus3Config=td.nexusNexus3ConfigReleases()
        assert nexus3Config
        Nexus3SearchAssetsConfig nexus3SearchAssetsConfig = td.nexus3SearchAssetsConfigThirdpartyBfopdf() 
        assert nexus3SearchAssetsConfig
        def nass = new Nexus3SearchAssets().search(nexus3Config, nexus3SearchAssetsConfig)
        assert nass
        
    }
}