package com.javapda.nexus3.nei.action
import com.javapda.nexus3.nei.domain.*

class Nexus3RepositoryAssetDownloaderTest extends GroovyTestCase {

    void testDownload() {
        File downloadedFile = new Nexus3RepositoryAssetDownloader().download(config(), asset(), destinationDirectory())
        assert downloadedFile
    }
    def config() {
        Nexus3Installation installation = Nexus3ExportImportPreferences.loadLocal().getInstallation("localhost-fake")
        new Nexus3Config([
            nexus3Url:installation.nexus3Url, //"http://localhost:8089",
            repoName:"thirdparty",
            credentials:installation.credentials]) //new LocalCredentialsProvider().get("gsadmin")])
        
    }
    def asset() {
        Nexus3RepositoryAsset n = Nexus3RepositoryAsset.fromJson(assetJson)
        assert n
        n
    }
    def destinationDirectory() {
        "/tmp"
    }
     
    static nexus3configjson = '''
    '''

    static assetJson = '''
        {
            "downloadUrl" : "http://localhost:8089/repository/thirdparty/bigfaceless/bfopdf-stamp/2.18.8/bfopdf-stamp-2.18.8.jar.sha1",
            "path" : "bigfaceless/bfopdf-stamp/2.18.8/bfopdf-stamp-2.18.8.jar.sha1",
            "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmNGE1ZjE0NjcwNWU0OTE0OA",
            "repository" : "thirdparty",
            "format" : "maven2",
            "checksum" : {
            "sha1" : "2561ae58469442f00018de28cb4b912442281427",
            "md5" : "d6ec9da10e1fb030295ad43b9e5bf010"
            }
        }     
    '''
}