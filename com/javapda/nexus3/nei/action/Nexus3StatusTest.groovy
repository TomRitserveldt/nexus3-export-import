package com.javapda.nexus3.nei.action
@Grapes(
    @Grab(group='org.junit.jupiter', module='junit-jupiter-api', version='5.6.2', scope='test')
)

import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.rest.*
import com.javapda.nexus3.urlproviders.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeEach

class Nexus3StatusTest { 
    Nexus3ExportImportPreferences preferences
    Nexus3Installation localInstallation

    @BeforeEach
    void setup() {
        assert preferences()
        assert localInstallation()
    }

    @Test
    void performStatus() {
        def responseHeaders = new Nexus3Status().getStatus(localInstallation.nexus3Url, localInstallation.credentials)
        assert responseHeaders
    }

    Nexus3ExportImportPreferences preferences() {
        if (!preferences) {
            preferences = Nexus3ExportImportPreferences.loadLocal()
            assert preferences
        }
        preferences
    }

    Nexus3Installation localInstallation() {
        if (!localInstallation) {
            localInstallation = preferences().getInstallation("localhost-fake")
        }
        localInstallation
    }



}