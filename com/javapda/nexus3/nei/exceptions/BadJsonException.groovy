package com.javapda.nexus3.nei.exceptions

class BadJsonException extends RuntimeException {

    BadJsonException(String message) {
        super(message)
    }
}