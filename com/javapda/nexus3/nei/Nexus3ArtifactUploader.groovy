package com.javapda.nexus3.nei
@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7.1' )
@Grab(group='org.apache.httpcomponents', module='httpclient', version='4.5.12')
@Grab(group='org.apache.httpcomponents', module='httpmime', version='4.5.7')
@Grab(group='org.apache.httpcomponents', module='httpcore', version='4.4.3')


import org.apache.http.entity.mime.*
import org.apache.http.impl.client.*
import static groovyx.net.http.Method.*
import groovy.util.slurpersupport.GPathResult
import org.apache.http.client.HttpResponseException
import org.apache.http.client.methods.*
import org.apache.http.entity.mime.content.*
import org.apache.http.entity.*
import org.apache.http.*
import groovyx.net.http.*

import static java.lang.String.format
import groovy.json.JsonSlurper
import groovy.transform.ToString
import com.javapda.nexus3.nei.cli.Nexus3ArtifactUploaderCLI
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.nei.action.*
import com.javapda.nexus3.nei.domain.config.*
import com.javapda.nexus3.rest.*
import com.javapda.nexus3.util.*
@ToString
class Nexus3ArtifactUploader {
    Nexus3ArtifactUploaderConfig config
    Nexus3ArtifactUploader(Nexus3ArtifactUploaderConfig config) {
        this.config = config
        verify()
    }

    def verify() {
        def reasons = []
        println("VERIFYING")
        if (!new WebSiteLiveVerifier(config.repoUrl).verify()) {
            reasons << "bad url ${config.repoUrl} - not live"
        }
        

        if (!reasons.empty) {
            throw new RuntimeException("unable to upload: ${reasons}")
        }
    }

    def upload() {
        
        if (new WebSiteLiveVerifier(config.repoUrl).verify()) {
            GAV gav = config.gav
            CloseableHttpClient httpclient = HttpClients.createDefault();
            assert httpclient
            File file = new File(config.filename)
            assert file.exists()
            MultipartEntityBuilder entitybuilder = MultipartEntityBuilder.create();
            assert entitybuilder
            entitybuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            entitybuilder.addBinaryBody("maven2.asset1", file)
            entitybuilder.addTextBody("maven2.groupId", gav.groupId, ContentType.MULTIPART_FORM_DATA);
            entitybuilder.addTextBody("maven2.artifactId", gav.artifactId, ContentType.MULTIPART_FORM_DATA);
            entitybuilder.addTextBody("maven2.version", gav.version, ContentType.TEXT_PLAIN);
            entitybuilder.addTextBody("maven2.packaging", gav.packaging, ContentType.MULTIPART_FORM_DATA);
            if (gav.classifier) {
                entitybuilder.addTextBody("maven2.asset1.classifier", gav.classifier, ContentType.MULTIPART_FORM_DATA);
            }
            entitybuilder.addTextBody("maven2.asset1.extension", gav.packaging, ContentType.MULTIPART_FORM_DATA);
            HttpEntity mutiPartHttpEntity = entitybuilder.build(); 
            assert mutiPartHttpEntity
            def repoUrl="${config.repoUrl}/service/rest/v1/components"
            def params = ["repository":config.repoName]

            def url = "${repoUrl}?${new QueryStringBuilder(params:params).get()}"
            Credentials credentials = config.credentials
            println("""
              gav:              ${gav}
              url:              ${url}
              credentials:      ${credentials}
              file:             ${file.absolutePath}
              repoName:         ${config.repoName}
            """)

            RequestBuilder reqbuilder = RequestBuilder.post(url);
            if (credentials) {
                reqbuilder.setHeader("Authorization",credentials.basicAuth)
            } else {
                println("No credentials provided")
            }
            reqbuilder.setEntity(mutiPartHttpEntity);
            HttpUriRequest multipartRequest = reqbuilder.build();
            assert multipartRequest
            HttpResponse httpresponse = httpclient.execute(multipartRequest);
            assert httpresponse
            println("httpresponse: ${httpresponse}")

            // throw new RuntimeException("TODO: actual upload in ${this.class}")
        } else {
            throw new RuntimeException("bad repoUrl: '${config.repoUrl}'")
        }
    }

    static main(args) {
            Nexus3ArtifactUploaderCLI cli = Nexus3ArtifactUploaderCLI.INSTANCE
            def allArgs = []
            // allArgs.addAll(prefs)
            allArgs.addAll(args)
            
            Nexus3ArtifactUploaderConfig config = cli.parse(allArgs)
            if (config.verbose) {
                println("args: ${args}")
                println("config: ${config}")
            }
            new Nexus3ArtifactUploader(config).upload()
    }    

}
