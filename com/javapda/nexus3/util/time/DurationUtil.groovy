package com.javapda.nexus3.util.time

class DurationUtil {

    String duration(Date startTime, Date endTime) {
        def delta=(long)((endTime.getTime()-startTime.getTime())/1000)
        return String.format("%02d:%02d:%02d", (delta/3600L).longValue(), (delta%3600L/60).longValue(), (delta%60L))
    }
}