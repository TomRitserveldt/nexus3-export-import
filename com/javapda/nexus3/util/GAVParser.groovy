package com.javapda.nexus3.util
import java.text.SimpleDateFormat
import com.javapda.nexus3.nei.domain.*


/**
https://e.printstacktrace.blog/groovy-regular-expressions-the-definitive-guide/
*/
class GAVParser {
    static def PATTERN_GAV = /^(\S+)\:(\S+)\:(\S+)\:(\S+)?\:(\S+)?$/
    // static def PATTERN_GAV = /^(\S+)\/(\S+)\/(\S+)\/((\2)-(\3)-?(\S*).(jar|pom|war|md5|sha1))$/
    def parse(String gavText) {
        def matches
        String groupId, artifactId, version, classifier, packaging
        if ((matches = gavText =~ PATTERN_GAV)) {
            showGavAndMatches(gavText,matches)
            groupId=matches.group(1)
            artifactId=matches.group(2)
            version=matches.group(3)
            classifier=matches.group(4)
            packaging=matches.group(5)
        } else {
            throw new RuntimeException("invalid non-compatible gavText '${gavText}'")
        }
        def gav = new GAV(
                    gavText:gavText,
                    groupId:groupId,
                    artifactId:artifactId,
                    version:version,
                    classifier:classifier,
                    packaging:packaging
                    )
        gav
    }

    def showGavAndMatches(gav,matches) {
        if (false) {
            println("matches.groupCount(): ${matches.groupCount()}")
            println("pattern: ${PATTERN_GAV}")
            println("gav ${gav}")
            for (int i=0; i<= matches.groupCount();i++) {
                println("${i} ${matches.group(i)}")
            }
        }

    }

}