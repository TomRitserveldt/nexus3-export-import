package com.javapda.nexus3.util
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true, includePackage=false)
class Nexus3UrlBuilderTest extends GroovyTestCase {
    void test() {
        def m = 
            new Nexus3UrlBuilder<Nexus3UrlBuilder>()
                .setNexus3Url("http://my.install.com/nexus")
                .setPath("service/rest/v1/assets")
                .addParam([jed:'clampett'])
        assert m
        assert m.build()=="http://my.install.com/nexus/service/rest/v1/assets?jed=clampett"

    }
}