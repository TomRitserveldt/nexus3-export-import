
package com.javapda.nexus3
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.nei.action.*
class Nexus3Api {
    // static listRepositories()

    String getVersion(String nexus3Url, Credentials credentials) {
        new Nexus3Version().getVersion(nexus3Url, credentials)

    }

}