package com.javapda.nexus3.rest
import groovy.transform.ToString
import groovy.json.JsonSlurper
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.rest.*
import com.javapda.nexus3.urlproviders.*
import com.javapda.nexus3.nei.action.*
import com.javapda.nexus3.nei.exceptions.*

@ToString(includeNames=true, includeFields=true, includePackage=false)
class ResponseHeadersGet {
    Credentials credentials
    String url
    boolean verbose

    Map<String, List<String>> get() {
        def connection = new URL( url )
                .openConnection() as HttpURLConnection
        
        if (!credentials.empty) {
            String basicAuth = 
                "Basic ${new String(Base64.encoder.encode(credentials.colonSeparatedText.getBytes()))}";
            connection.setRequestProperty ("Authorization", basicAuth);
        } else {
            println("no credentials")
        }

        // set some headers
        connection.setRequestProperty( 'User-Agent', 'javapda-groovy-2.4.4' )
        connection.setRequestProperty( 'Accept', 'application/json' )
        // get the response code - automatically sends the request
        Map<String, List<String>> responseHeadersMap = connection.getHeaderFields()
        if (verbose) {
            println("response headers: ${responseHeadersMap}")
        }
        responseHeadersMap
    }

}