package com.package.nexus3.rest
@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7.1' )
@Grab(group='org.apache.httpcomponents', module='httpclient', version='4.5.12')
@Grab(group='org.apache.httpcomponents', module='httpmime', version='4.5.7')
@Grab(group='org.apache.httpcomponents', module='httpcore', version='4.4.3')


import com.javapda.nexus3.nei.domain.*
import org.apache.http.entity.mime.*
import org.apache.http.impl.client.*
import static groovyx.net.http.Method.*
import groovy.util.slurpersupport.GPathResult
import org.apache.http.client.HttpResponseException
import com.javapda.nexus3.rest.*
import org.apache.http.client.methods.*
import org.apache.http.entity.mime.content.*
import org.apache.http.entity.*
import org.apache.http.*
import groovyx.net.http.*


/**
https://github.com/jgritman/httpbuilder/blob/master/src/test/groovy/groovyx/net/http/HTTPBuilderTest.groovy
*/
class HttpBuilderTest extends GroovyTestCase {
    public void DIStest404() {
        new HTTPBuilder().request('http://www.google.com',GET,TEXT) {
            uri.path = '/asdfg/asasdfs' // should produce 404
            response.'404' = {
                println 'got expected 404!'
            }
            response.success = {
                assert false // should not have succeeded
            }
        }
    }

    /**
    https://www.baeldung.com/httpclient-multipart-upload

        curl -X POST "http://localhost:8089/service/rest/v1/components?repository=thirdparty" -H "accept: application/json" -H "Content-Type: multipart/form-data" -F "maven2.groupId=com.javapda" -F "maven2.artifactId=pom-fake" -F "maven2.version=0.0.1.RELEASE" -F "maven2.packaging=pom" -F "maven2.asset1=@pom-fake.xml;type=text/xml" -F "maven2.asset1.extension=xml"    
        request url:
        http://localhost:8089/service/rest/v1/components?repository=thirdparty

        Response headers:
            date: Tue, 31 Mar 2020 19:13:10 GMT 
            server: Nexus/3.21.2-03 (OSS) 
            x-content-type-options: nosniff 
    */
    void XtestPostToNexus() {
        def params = [
            "repository":"thirdparty",
            // "maven2.groupId":"com.javapda",
            // "maven2.artifactId":"pom-fake",
            // "maven2.version":"0.0.1.RELEASE",
            // "maven2.packaging":"pom",
            // "maven2.asset1":"@pom-fake.xml;type=text/xml",
            // "maven2.asset1.extension":"xml"
        ]
        def repoUrl="http://localhost:8089/service/rest/v1/components"
        def url = "${repoUrl}?${new QueryStringBuilder(params:params).get()}"
        println(url)
        File file = new File("./docs/pom-fake.xml")
        assert file.exists()
        HttpPost post = new HttpPost(repoUrl);
        assert post
        // FileBody fileBody = new FileBody(file, ContentType.DEFAULT_BINARY);
        // assert fileBody
        // StringBody stringBody1 = new StringBody("Message 1", ContentType.MULTIPART_FORM_DATA);
        // assert stringBody1

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        // builder.addBinaryBody("upfile", file, ContentType.DEFAULT_BINARY, file.name)
        builder.addTextBody("maven2.groupId", "com.javapda", ContentType.MULTIPART_FORM_DATA);
        builder.addTextBody("maven2.artifactId", "pom-fake", ContentType.MULTIPART_FORM_DATA);
        builder.addTextBody("maven2.version", "0.0.1.RELEASE", ContentType.MULTIPART_FORM_DATA);
        builder.addTextBody("maven2.packaging", "pom", ContentType.MULTIPART_FORM_DATA);
        builder.addTextBody("maven2.asset1.extension", "pom", ContentType.MULTIPART_FORM_DATA);
        builder.addBinaryBody("maven2.asset1", file, ContentType.MULTIPART_FORM_DATA, file.name)
        HttpEntity entity = builder.build();
        post.setEntity(entity);
        Credentials credentials = new Credentials("trustedclient:trustedclient123")
        post.setHeader("Authorization",credentials.basicAuth)
        // post.setHeader("Accept","application/json:")
        CloseableHttpClient client = 
                        HttpClientBuilder.create().disableRedirectHandling().build();
        HttpResponse response = client.execute(post);
        println(response)

    }

    /**
        https://www.tutorialspoint.com/apache_httpclient/apache_httpclient_multipart_upload.htm
    */
    void testOther() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        assert httpclient
        File file = new File("./docs/pom-fake.xml")
        assert file.exists()
        MultipartEntityBuilder entitybuilder = MultipartEntityBuilder.create();
        assert entitybuilder
        entitybuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        entitybuilder.addBinaryBody("maven2.asset1", file)
        entitybuilder.addTextBody("maven2.groupId", "com.javapda", ContentType.MULTIPART_FORM_DATA);
        entitybuilder.addTextBody("maven2.artifactId", "GUGGUpom-fake", ContentType.MULTIPART_FORM_DATA);
        entitybuilder.addTextBody("maven2.version", "0.0.39.RELEASE", ContentType.TEXT_PLAIN);
        entitybuilder.addTextBody("maven2.packaging", "pom", ContentType.MULTIPART_FORM_DATA);
        entitybuilder.addTextBody("maven2.asset1.extension", "pom", ContentType.MULTIPART_FORM_DATA);
        HttpEntity mutiPartHttpEntity = entitybuilder.build(); 
        assert mutiPartHttpEntity
        def repoUrl="http://localhost:8089/service/rest/v1/components"
        def params = ["repository":"thirdparty"]

        def url = "${repoUrl}?${new QueryStringBuilder(params:params).get()}"
        println(url)

        RequestBuilder reqbuilder = RequestBuilder.post(url);
        Credentials credentials = new Credentials("trustedclient:trustedclient123")
        reqbuilder.setHeader("Authorization",credentials.basicAuth)
        reqbuilder.setEntity(mutiPartHttpEntity);
        HttpUriRequest multipartRequest = reqbuilder.build();
        assert multipartRequest
        HttpResponse httpresponse = httpclient.execute(multipartRequest);
        assert httpresponse
        println("httpresponse: ${httpresponse}")
    }
}