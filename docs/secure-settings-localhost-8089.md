# secure-settings-localhost-8089

## overview
* demonstrate ability to deploy using [mvn](https://books.sonatype.com/mvnref-book/reference/running-sect-options.html)
* trustedclient-repo is a group repository configured on the local nexus3

## settings file:  [secure-settings-localhost-8089.md](secure-settings-localhost-secure-settings-localhost-8089.xml)
* has the settings for install and deployment

## overriding <distributionManagement> of the pom.xml called [pom-localhost-8089.xml](pom-localhost-8089.xml)


### deploy
```
mvn --file pom-localhost-8089.xml --settings ${HOME}/.m2/secure-settings-localhost-8089.xml deploy
```

### in nexus
* deploy once
  
![deployed to local nexus with authentication](../images/pom-fake-localhost-deployed-to-8089.png)
* deploy 2nd

![after deploying 2nd time to local nexus with authentication](../images/pom-fake-localhost-deployed-to-8089-after-2nd-deployment.png)