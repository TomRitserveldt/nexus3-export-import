# secure-settings-localhost-8089

## overview
* demonstrate ability to deploy using [mvn](https://books.sonatype.com/mvnref-book/reference/running-sect-options.html)
* trustedclient-repo is a group repository configured on the local nexus3

## settings file:  [secure-settings-localhost-8089.md](secure-settings-localhost-secure-settings-localhost-8089.xml)
* has the settings for install and deployment

## overriding <distributionManagement> of the pom.xml called [pom-localhost-8089.xml](pom-localhost-8089.xml)


### deploy
```
mvn -Dnexus-repository-url=https://localhost:8089 --file pom-gs.xml --settings ${HOME}/.m2/secure-settings-gs.xml deploy
```

### in nexus
* deploy once

    ![after one deployment](../images/pom-fake-gs-deployed-to-nexus.png "after one deployment")

* deploy twice

    ![after one deployment](../images/pom-fake-gs-deployed-to-nexus-after-2nd-deployment.png "after 2nd deployment")
  
